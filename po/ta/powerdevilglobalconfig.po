# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the powerdevil package.
#
# Kishore G <kishore96@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-17 00:47+0000\n"
"PO-Revision-Date: 2022-02-27 19:21+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "கோ. கிஷோர்"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: GeneralPage.cpp:108
#, kde-format
msgid "Do nothing"
msgstr "ஒன்றும் செய்யாதே"

#: GeneralPage.cpp:110
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "தூங்கு"

#: GeneralPage.cpp:113
#, kde-format
msgid "Hibernate"
msgstr "ஆழுறங்கு"

#: GeneralPage.cpp:115
#, kde-format
msgid "Shut down"
msgstr "கணினியை நிறுத்து"

#: GeneralPage.cpp:264
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "ஆற்றல் மேலாண்மைக்கான பின்னணி சேவை இயங்குவது போல் தெரியவில்லை."

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr "<b>மின்கல நிலைகள்</b>"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, kde-format
msgid "&Low level:"
msgstr "&குறைவான நிலை:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "மின்கலம் குறைவாயுள்ளதாய் கருதப்படும் நிலை"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr "மின்கல ஆற்றல் இந்த அளவுக்கு கீழ் இருந்தால் குறைவாயுள்ளதாக கருதப்படும்"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, kde-format
msgid "&Critical level:"
msgstr "&நெருக்கடியான நிலை:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "மின்கலம் நெருக்கடியாயுள்ளதாய் கருதப்படும் நிலை"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr "மின்கல ஆற்றல் இந்த அளவுக்கு கீழ் இருந்தால் நெருக்கடியாக உள்ளதாக கருதப்படும்"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, kde-format
msgid "A&t critical level:"
msgstr "நெ&ருக்கடியான நிலையில்:"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr "இணைக்கப்பட்ட சாதனங்களுக்கான குறைவான நிலை:"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr "மின்னேற்ற வரம்பு"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""
"<html><head/><body><p>நீண்ட காலத்திற்கு மின்கலத்தை 100%-த்தில் வைப்பது, அதனை "
"சேதப்படுத்தலாம். மின்கலத்தின் அதிகப்படி ஆற்றலை வரம்புப்படுத்துவதன் மூலம், அதன் ஆயுளை நீங்கள் "
"நீடிக்கலாம்.</p></body></html>"

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"மின்கலத்தில் மறுபடியும் மின்னேறுமாறு செய்ய, மின்னூற்றை கழற்றி திரும்ப மாட்ட "
"வேண்டியிருக்கலாம்."

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr "இதற்கு கீழ் வந்தால் மட்டும் மின்னேறுவதை துவக்கு:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr "செருகப்பட்டிருக்கும்போது எப்போதும் மின்னேற்று"

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr "கணினி உறங்குவதற்கு முன் ஊடக இயக்கிகளை இடைநிறுத்துவது:"

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr "வேண்டும்"

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr "அறிவிப்புகளை அமை…"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr "மற்ற அமைப்புகள்"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr "மின்னேறுவதை இந்நிலையில் நிறுத்து:"
