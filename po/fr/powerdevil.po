# translation of powerdevil.po to Français
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Julien Richard-Foy <julien.rf@no-log.org>, 2008, 2009, 2010.
# Sébastien Renard <Sebastien.Renard@digitalfox.org>, 2009, 2012.
# xavier <ktranslator31@yahoo.fr>, 2013, 2014.
# Maxime Corteel <mcorteel@gmail.com>, 2015.
# Vincent Pinon <vpinon@kde.org>, 2016, 2017.
# Simon Depiets <sdepiets@gmail.com>, 2019, 2020.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-03 00:46+0000\n"
"PO-Revision-Date: 2022-09-04 10:49+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.08.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""
"Xavier Besnard, Julien Richard-Foy, Maxime Corteel, Vincent Pinon, Simon "
"Depiets"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"xavier@besnard@neuf.fr, julien.rf@no-log.org, mcorteel@gmail.com, vpinon@kde."
"org, sdepiets@gmail.com"

#: actions/bundled/brightnesscontrol.cpp:53 actions/bundled/dpms.cpp:79
#: actions/bundled/handlebuttonevents.cpp:57
#: actions/bundled/keyboardbrightnesscontrol.cpp:57
#, kde-format
msgctxt "Name for powerdevil shortcuts category"
msgid "Power Management"
msgstr "Gestion de l'énergie"

#: actions/bundled/brightnesscontrol.cpp:56
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness"
msgstr "Augmenter la luminosité de l'écran"

#: actions/bundled/brightnesscontrol.cpp:61
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness"
msgstr "Diminuer la luminosité de l'écran"

#: actions/bundled/brightnesscontrolconfig.cpp:75
#, kde-format
msgctxt "Brightness level, label for the slider"
msgid "Level"
msgstr "Niveau"

#: actions/bundled/dimdisplayconfig.cpp:64 actions/bundled/dpmsconfig.cpp:60
#: actions/bundled/runscriptconfig.cpp:82
#: actions/bundled/suspendsessionconfig.cpp:86
#, kde-format
msgid " min"
msgstr "min"

#: actions/bundled/dimdisplayconfig.cpp:74
#: actions/bundled/runscriptconfig.cpp:85
#: actions/bundled/runscriptconfig.cpp:106
#, kde-format
msgid "After"
msgstr "Après"

#: actions/bundled/dpms.cpp:82
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Turn Off Screen"
msgstr "Éteindre l'écran"

#: actions/bundled/dpmsconfig.cpp:61
#, kde-format
msgid "Switch off after"
msgstr "Éteindre après"

#: actions/bundled/handlebuttonevents.cpp:62
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Suspend"
msgstr "Suspendre"

#: actions/bundled/handlebuttonevents.cpp:67
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Hibernate"
msgstr "Hiberner"

#: actions/bundled/handlebuttonevents.cpp:72
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Power Off"
msgstr "Éteindre"

#: actions/bundled/handlebuttonevents.cpp:86
#, kde-format
msgctxt ""
"@action:inmenu Global shortcut, used for long presses of the power button"
msgid "Power Down"
msgstr "Forcer l'extinction"

#: actions/bundled/handlebuttoneventsconfig.cpp:87
#, kde-format
msgctxt "Execute action on lid close even when external monitor is connected"
msgid "Even when an external monitor is connected"
msgstr "Même lorsqu'un écran externe est connecté"

#: actions/bundled/handlebuttoneventsconfig.cpp:97
#, kde-format
msgid "Do nothing"
msgstr "Ne rien faire"

#: actions/bundled/handlebuttoneventsconfig.cpp:99
#: actions/bundled/suspendsessionconfig.cpp:90
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Veille"

#: actions/bundled/handlebuttoneventsconfig.cpp:102
#: actions/bundled/suspendsessionconfig.cpp:93
#, kde-format
msgid "Hibernate"
msgstr "En hibernation"

#: actions/bundled/handlebuttoneventsconfig.cpp:105
#: actions/bundled/suspendsessionconfig.cpp:96
#, kde-format
msgid "Hybrid sleep"
msgstr "Mise en veille hybride"

#: actions/bundled/handlebuttoneventsconfig.cpp:107
#: actions/bundled/suspendsessionconfig.cpp:98
#, kde-format
msgid "Shut down"
msgstr "Éteindre"

#: actions/bundled/handlebuttoneventsconfig.cpp:108
#: actions/bundled/suspendsessionconfig.cpp:99
#, kde-format
msgid "Lock screen"
msgstr "Verrouiller l'écran"

#: actions/bundled/handlebuttoneventsconfig.cpp:110
#, kde-format
msgid "Prompt log out dialog"
msgstr "Afficher la fenêtre de déconnexion"

#: actions/bundled/handlebuttoneventsconfig.cpp:112
#, kde-format
msgid "Turn off screen"
msgstr "Éteindre l'écran"

#: actions/bundled/handlebuttoneventsconfig.cpp:138
#, kde-format
msgid "When laptop lid closed"
msgstr "Lorsque l'écran du portable est rabattu"

#: actions/bundled/handlebuttoneventsconfig.cpp:149
#, kde-format
msgid "When power button pressed"
msgstr "Lorsque le bouton de mise en marche est enfoncé"

#: actions/bundled/keyboardbrightnesscontrol.cpp:61
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Keyboard Brightness"
msgstr "Augmenter la luminosité du clavier"

#: actions/bundled/keyboardbrightnesscontrol.cpp:66
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Keyboard Brightness"
msgstr "Diminuer la luminosité du clavier"

#: actions/bundled/keyboardbrightnesscontrol.cpp:71
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Toggle Keyboard Backlight"
msgstr "Changer le rétro-éclairage du clavier"

#: actions/bundled/keyboardbrightnesscontrolconfig.cpp:77
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Level"
msgstr "Niveau"

#: actions/bundled/powerprofileconfig.cpp:84
#: actions/bundled/wirelesspowersavingconfig.cpp:75
#: actions/bundled/wirelesspowersavingconfig.cpp:80
#: actions/bundled/wirelesspowersavingconfig.cpp:85
#, kde-format
msgid "Leave unchanged"
msgstr "Laisser inchangé"

#: actions/bundled/powerprofileconfig.cpp:92
#, kde-format
msgid "Power Save"
msgstr "Économie d'énergie"

#: actions/bundled/powerprofileconfig.cpp:93
#, kde-format
msgid "Balanced"
msgstr "Équilibré"

#: actions/bundled/powerprofileconfig.cpp:94
#, kde-format
msgid "Performance"
msgstr "Performance"

#: actions/bundled/powerprofileconfig.cpp:109
#, kde-format
msgctxt "Switch to power management profile"
msgid "Switch to:"
msgstr "Basculer vers :"

#: actions/bundled/runscriptconfig.cpp:71
#, kde-format
msgid "Script"
msgstr "Script"

#: actions/bundled/runscriptconfig.cpp:83
#, kde-format
msgid "On Profile Load"
msgstr "Au chargement du profil"

#: actions/bundled/runscriptconfig.cpp:84
#, kde-format
msgid "On Profile Unload"
msgstr "Au déchargement du profil"

#: actions/bundled/runscriptconfig.cpp:95
#, kde-format
msgid "Run script"
msgstr "Exécuter le script"

#: actions/bundled/suspendsessionconfig.cpp:87
#, kde-format
msgid "after "
msgstr "après "

#: actions/bundled/suspendsessionconfig.cpp:108
#, kde-format
msgid "Automatically"
msgstr "Automatiquement"

#: actions/bundled/suspendsessionconfig.cpp:115
#, kde-format
msgid "While asleep, hibernate after a period of inactivity"
msgstr "En veille, hiberner après une certaine période d'inactivité"

#: actions/bundled/wirelesspowersavingconfig.cpp:76
#: actions/bundled/wirelesspowersavingconfig.cpp:81
#: actions/bundled/wirelesspowersavingconfig.cpp:86
#, kde-format
msgid "Turn off"
msgstr "Éteindre"

#: actions/bundled/wirelesspowersavingconfig.cpp:77
#: actions/bundled/wirelesspowersavingconfig.cpp:82
#: actions/bundled/wirelesspowersavingconfig.cpp:87
#, kde-format
msgid "Turn on"
msgstr "Allumer"

#: actions/bundled/wirelesspowersavingconfig.cpp:110
#, kde-format
msgid "Wi-Fi"
msgstr "Wifi"

#: actions/bundled/wirelesspowersavingconfig.cpp:111
#, kde-format
msgid "Mobile broadband"
msgstr "Téléphonie large bande"

#: actions/bundled/wirelesspowersavingconfig.cpp:112
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: backends/upower/login1suspendjob.cpp:79
#: backends/upower/upowersuspendjob.cpp:69
#, kde-format
msgid "Unsupported suspend method"
msgstr "Méthode de mise en veille non prise en charge"

#: powerdevilapp.cpp:62
#, kde-format
msgid "KDE Power Management System"
msgstr "Système de gestion de l'énergie pour KDE"

#: powerdevilapp.cpp:63
#, kde-format
msgctxt "@title"
msgid ""
"PowerDevil, an advanced, modular and lightweight power management daemon"
msgstr ""
"PowerDevil est un démon de gestion de l'énergie avancé, modulaire et léger."

#: powerdevilapp.cpp:65
#, kde-format
msgctxt "@info:credit"
msgid "(c) 2015-2019 Kai Uwe Broulik"
msgstr "(c) 2015-2019 Kai Uwe Broulik"

#: powerdevilapp.cpp:66
#, kde-format
msgctxt "@info:credit"
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: powerdevilapp.cpp:67
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "Mainteneur"

#: powerdevilapp.cpp:69
#, kde-format
msgctxt "@info:credit"
msgid "Dario Freddi"
msgstr "Dario Freddi"

#: powerdevilapp.cpp:70
#, kde-format
msgctxt "@info:credit"
msgid "Previous maintainer"
msgstr "Mainteneur précédent"

#: powerdevilapp.cpp:160
#, kde-format
msgid "Replace an existing instance"
msgstr "Remplacer une instance existante"

#: powerdevilcore.cpp:435 powerdevilcore.cpp:447
#, kde-format
msgid "Activity Manager"
msgstr "Gestionnaire d'activités"

#: powerdevilcore.cpp:436
#, kde-format
msgid "This activity's policies prevent the system from going to sleep"
msgstr "Ces règles d'activités empêchent le système d'être mis en veille"

#: powerdevilcore.cpp:448
#, kde-format
msgid "This activity's policies prevent screen power management"
msgstr "Ces règles d'activité empêchent la gestion de l'énergie de l'écran"

#: powerdevilcore.cpp:511
#, kde-format
msgid "Extra Battery Added"
msgstr "Batterie supplémentaire ajoutée"

#: powerdevilcore.cpp:512 powerdevilcore.cpp:722
#, kde-format
msgid "The computer will no longer go to sleep."
msgstr "L'ordinateur ne sera plus mis en veille."

#: powerdevilcore.cpp:572
#, kde-format
msgctxt "%1 is vendor name, %2 is product name"
msgid "%1 %2"
msgstr "%1 %2"

#: powerdevilcore.cpp:575
#, kde-format
msgctxt "The battery in an external device"
msgid "Device Battery Low (%1% Remaining)"
msgstr "Batterie du périphérique faible (%1 % restant)"

#: powerdevilcore.cpp:577
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in \"%1\" is running low, and the device may turn off at any "
"time. Please recharge or replace the battery."
msgstr ""
"La batterie dans « %1 ») est faible et le périphérique risque de s'éteindre "
"à tout moment. Veuillez recharger ou remplacer la batterie."

#: powerdevilcore.cpp:583
#, kde-format
msgid "Mouse Battery Low (%1% Remaining)"
msgstr "Batterie de la souris faible (%1 % restant)"

#: powerdevilcore.cpp:587
#, kde-format
msgid "Keyboard Battery Low (%1% Remaining)"
msgstr "Batterie du clavier faible (%1 % restant)"

#: powerdevilcore.cpp:591
#, kde-format
msgid "Bluetooth Device Battery Low (%1% Remaining)"
msgstr "Batterie du périphérique Bluetooth faible (%1 % restant)"

#: powerdevilcore.cpp:593
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in Bluetooth device \"%1\" is running low, and the device may "
"turn off at any time. Please recharge or replace the battery."
msgstr ""
"La batterie de l'appareil Bluetooth « %1 » est faible et le périphérique "
"risque de s'éteindre à tout moment. Veuillez remplacer ou charger la "
"batterie."

#: powerdevilcore.cpp:639
#, kde-format
msgid ""
"Battery running low - to continue using your computer, make sure that the "
"power adapter is plugged in and that it provides enough power."
msgstr ""
"La batterie est presque vide. Pour continuer à utiliser votre ordinateur, "
"veuillez vous assurer que l'adaptateur secteur est branché et qu'il fournit "
"assez de puissance."

#: powerdevilcore.cpp:641
#, kde-format
msgid ""
"Battery running low - to continue using your computer, plug it in or shut it "
"down and change the battery."
msgstr ""
"Batterie faible. Pour continuer à utiliser votre ordinateur, branchez-le sur "
"le secteur ou arrêtez-le et changez de batterie."

#: powerdevilcore.cpp:658
#, kde-format
msgctxt ""
"Cancel timeout that will automatically put system to sleep because of low "
"battery"
msgid "Cancel"
msgstr "Annuler"

#: powerdevilcore.cpp:667
#, kde-format
msgid "Battery level critical. Your computer will shut down in 60 seconds."
msgstr "Batterie critique. Votre ordinateur s'éteindra dans 60 secondes."

#: powerdevilcore.cpp:672
#, kde-format
msgid ""
"Battery level critical. Your computer will enter hibernation mode in 60 "
"seconds."
msgstr ""
"Batterie critique. Votre ordinateur passera en hibernation dans 60 secondes."

#: powerdevilcore.cpp:677
#, kde-format
msgid "Battery level critical. Your computer will go to sleep in 60 seconds."
msgstr ""
"Batterie critique. Votre ordinateur sera mis en veille dans 60 secondes."

# unreviewed-context
#: powerdevilcore.cpp:682
#, kde-format
msgid "Battery level critical. Please save your work."
msgstr "Batterie critique. Veuillez enregistrer vos travaux."

#: powerdevilcore.cpp:693
#, kde-format
msgid "Battery Low (%1% Remaining)"
msgstr "Batterie faible (%1 % restant)"

#: powerdevilcore.cpp:697
#, kde-format
msgid "Battery Critical (%1% Remaining)"
msgstr "Niveau de la batterie critique (%1 % restant)"

#: powerdevilcore.cpp:721
#, kde-format
msgid "AC Adapter Plugged In"
msgstr "Prise d'alimentation branchée"

#: powerdevilcore.cpp:724
#, kde-format
msgid "Running on AC power"
msgstr "Fonctionnement sur secteur"

#: powerdevilcore.cpp:724
#, kde-format
msgid "The power adapter has been plugged in."
msgstr "La prise d'alimentation a été branchée."

#: powerdevilcore.cpp:727
#, kde-format
msgid "Running on Battery Power"
msgstr "Fonctionnement sur batterie"

#: powerdevilcore.cpp:727
#, kde-format
msgid "The power adapter has been unplugged."
msgstr "La prise d'alimentation a été débranchée."

#: powerdevilcore.cpp:788
#, kde-format
msgid "Charging Complete"
msgstr "Charge complète"

#: powerdevilcore.cpp:788
#, kde-format
msgid "Battery now fully charged."
msgstr "La batterie est maintenant complètement chargée."

#~ msgid "Can't open file"
#~ msgstr "Impossible d'ouvrir le fichier"

#~ msgid ""
#~ "No valid Power Management backend plugins available. A new installation "
#~ "might solve this problem."
#~ msgstr ""
#~ "Aucun module externe n'est disponible pour le moteur de gestion de "
#~ "l'énergie. Une nouvelle installation devrait pouvoir régler ce problème."

# unreviewed-context
#~ msgid ""
#~ "Profile \"%1\" has been selected but does not exist.\n"
#~ "Please check your PowerDevil configuration."
#~ msgstr ""
#~ "Le profil « %1 » a été sélectionné mais il n'existe pas.\n"
#~ "Veuillez vérifier votre configuration de PowerDevil."

# unreviewed-context
#~ msgid ""
#~ "Could not connect to battery interface.\n"
#~ "Please check your system configuration"
#~ msgstr ""
#~ "Impossible de se connecter à l'interface de la batterie.\n"
#~ "Veuillez vérifier la configuration de votre système"

#~ msgid ""
#~ "The KDE Power Management System could not be initialized. The backend "
#~ "reported the following error: %1\n"
#~ "Please check your system configuration"
#~ msgstr ""
#~ "Impossible d'initialiser le système de gestion de l'énergie de KDE. Le "
#~ "moteur a rapporté l'erreur suivante : %1\n"
#~ "Veuillez vérifier la configuration de votre système."

#~ msgid "All pending suspend actions have been canceled."
#~ msgstr "Toute action de mise en veille en cours a été annulée."

#~ msgctxt "Placeholder is device name"
#~ msgid ""
#~ "The battery in your keyboard (\"%1\") is low, and the device may turn "
#~ "itself off at any time. Please replace or charge the battery as soon as "
#~ "possible."
#~ msgstr ""
#~ "La batterie de votre clavier (« %1 ») est faible, et le périphérique "
#~ "risque de s'éteindre à tout moment. Veuillez remplacer ou charger la "
#~ "batterie dès que possible."

#~ msgctxt "Placeholder is device name"
#~ msgid ""
#~ "The battery in a connected device (\"%1\") is low, and the device may "
#~ "turn itself off at any time. Please replace or charge the battery as soon "
#~ "as possible."
#~ msgstr ""
#~ "La batterie du périphérique connecté (« %1 ») est faible, et le "
#~ "périphérique risque de s'éteindre à tout moment. Veuillez remplacer ou "
#~ "charger la batterie dès que possible."

#~ msgid ""
#~ "Your battery level is critical, the computer will be suspended in 60 "
#~ "seconds."
#~ msgstr ""
#~ "Votre batterie a atteint un niveau critique, l'ordinateur sera mis en "
#~ "veille dans 60 secondes."

#~ msgid "Hybrid suspend"
#~ msgstr "Mise en veille hybride"

#~ msgid "Suspend"
#~ msgstr "Suspendre"

#~ msgctxt "@action:inmenu Global shortcut"
#~ msgid "Sleep"
#~ msgstr "En veille"

#~ msgid ""
#~ "Your battery capacity is %1%. This means your battery is broken and needs "
#~ "a replacement. Please contact your hardware vendor for more details."
#~ msgstr ""
#~ "La capacité de votre batterie est de %1 %. Cela signifie que votre "
#~ "batterie est en panne et nécessite un remplacement. Veuillez contacter "
#~ "votre fournisseur de matériel pour plus d'informations."

#~ msgid ""
#~ "One of your batteries (ID %2) has a capacity of %1%. This means it is "
#~ "broken and needs a replacement. Please contact your hardware vendor for "
#~ "more details."
#~ msgstr ""
#~ "La capacité de l'une de vos batteries (identifiant « %2 ») est de %1 %. "
#~ "Cela signifie que cette batterie est en panne et nécessite un "
#~ "remplacement. Veuillez contacter votre fournisseur de matériel pour plus "
#~ "d'informations."

#~ msgid "Broken Battery"
#~ msgstr "Batterie défectueuse"

#~ msgid ""
#~ "Your battery might have been recalled by %1. Usually, when vendors recall "
#~ "the hardware, it is because of factory defects which are usually eligible "
#~ "for a free repair or substitution. Please check <a href=\"%2\">%1's "
#~ "website</a> to verify if your battery is faulted."
#~ msgstr ""
#~ "Votre batterie a pu être rappelée par %1. Habituellement, un défaut "
#~ "d'usine est la raison pour laquelle les fabricants rappellent du matériel "
#~ "et ceci généralement pour un remplacement ou une réparation gratuite. "
#~ "Veuillez vérifier le <a href=\"%2\">site Internet de %1</a> pour savoir "
#~ "si votre batterie est éligible."

#~ msgid ""
#~ "One of your batteries (ID %3) might have been recalled by %1. Usually, "
#~ "when vendors recall the hardware, it is because of factory defects which "
#~ "are usually eligible for a free repair or substitution. Please check <a "
#~ "href=\"%2\">%1's website</a> to verify if your battery is faulted."
#~ msgstr ""
#~ "L'une de vos batteries (identifiant « %3 ») a pu être rappelée par %1. "
#~ "Habituellement, un défaut d'usine est la raison pour laquelle les "
#~ "fabricants rappellent du matériel et ceci généralement pour un "
#~ "remplacement ou une réparation gratuite. Veuillez vérifier le <a href="
#~ "\"%2\">site Internet de %1</a> pour savoir si votre batterie est éligible."

#~ msgid "Check Your Battery"
#~ msgstr "Vérifier votre batterie"

#~ msgid ""
#~ "Your Power Profiles have been updated to be used with the new KDE Power "
#~ "Management System. You can tweak them or generate a new set of defaults "
#~ "from System Settings."
#~ msgstr ""
#~ "Vos profils d'énergie ont été mis à jour pour être utilisé avec le "
#~ "nouveau système de gestion de l'énergie de KDE. Vous pouvez les "
#~ "paramétrer ou générer de nouveaux paramètres par défaut depuis la "
#~ "configuration du système."
