# Translation of powerdevilactivitiesconfig.po to Ukrainian
# Copyright (C) 2011-2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2011, 2015, 2016, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilactivitiesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-20 00:47+0000\n"
"PO-Revision-Date: 2022-08-20 08:38+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Юрій Чорноіван"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "yurchor@ukr.net"

#: activitypage.cpp:72
#, kde-format
msgid ""
"The activity service is running with bare functionalities.\n"
"Names and icons of the activities might not be available."
msgstr ""
"Ця служба просторів дій працює у режимі обмежених можливостей.\n"
"Можливо, не буде показано назви і піктограми просторів дій."

#: activitypage.cpp:146
#, kde-format
msgid ""
"The activity service is not running.\n"
"It is necessary to have the activity manager running to configure activity-"
"specific power management behavior."
msgstr ""
"Службу просторів дій не запущено.\n"
"Для окремого налаштування керування живленням у просторах дій потрібно "
"запустити керування просторами дій."

#: activitypage.cpp:242
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Здається, службу керування живленням не запущено."

#: activitywidget.cpp:98
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Призупинити"

#: activitywidget.cpp:102
#, kde-format
msgid "Hibernate"
msgstr "Приспати"

#: activitywidget.cpp:104
#, kde-format
msgid "Shut down"
msgstr "Вимкнути"

#: activitywidget.cpp:108
#, kde-format
msgid "PC running on AC power"
msgstr "Живлення здійснюється з мережі"

#: activitywidget.cpp:109
#, kde-format
msgid "PC running on battery power"
msgstr "Живлення здійснюється з акумуляторів"

#: activitywidget.cpp:110
#, kde-format
msgid "PC running on low battery"
msgstr "Рівень заряду акумуляторів низький"

#: activitywidget.cpp:139
#, kde-format
msgctxt "This is meant to be: Act like activity %1"
msgid "Activity \"%1\""
msgstr "Простір дій «%1»"

#. i18n: ectx: property (text), widget (QRadioButton, noSettingsRadio)
#: activityWidget.ui:19
#, kde-format
msgid "Do not use special settings"
msgstr "Не використовувати особливі параметри"

#. i18n: ectx: property (text), widget (QRadioButton, actLikeRadio)
#: activityWidget.ui:31
#, kde-format
msgid "Act like"
msgstr "Вважати, що"

#. i18n: ectx: property (text), widget (QRadioButton, specialBehaviorRadio)
#: activityWidget.ui:60
#, kde-format
msgid "Define a special behavior"
msgstr "Визначити особливу поведінку"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownScreenBox)
#: activityWidget.ui:72
#, kde-format
msgid "Never turn off the screen"
msgstr "Ніколи не вимикати екран"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownPCBox)
#: activityWidget.ui:79
#, kde-format
msgid "Never shut down the computer or let it go to sleep"
msgstr "Ніколи не вимикати комп'ютер і не переводити його до стану сну"

#. i18n: ectx: property (text), widget (QCheckBox, alwaysBox)
#: activityWidget.ui:91
#, kde-format
msgid "Always"
msgstr "Завжди"

#. i18n: ectx: property (text), widget (QLabel, alwaysAfterLabel)
#: activityWidget.ui:101
#, kde-format
msgid "after"
msgstr "за"

#. i18n: ectx: property (suffix), widget (QSpinBox, alwaysAfterSpin)
#: activityWidget.ui:108
#, kde-format
msgid " min"
msgstr " хв."

#. i18n: ectx: property (text), widget (QRadioButton, separateSettingsRadio)
#: activityWidget.ui:138
#, kde-format
msgid "Use separate settings"
msgstr "Використовувати окремі параметри"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Здається, службу керування живленням не запущено.\n"
#~ "Цю проблему можна вирішити запуском служби або додаванням служби до "
#~ "списку запуску у модулі «Запуск і вихід»"

#~ msgid "Suspend"
#~ msgstr "Призупинити"

#~ msgid "Never suspend or shutdown the computer"
#~ msgstr "Ніколи не присипляти і не вимикати комп’ютер"

#~ msgid "Activities Power Management Configuration"
#~ msgstr "Налаштування керування живленням у просторах дій"

#~ msgid "A per-activity configurator of KDE Power Management System"
#~ msgstr ""
#~ "Засіб налаштування системи керування живленням KDE для окремих просторів "
#~ "дій"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "© Dario Freddi, 2010"

#~ msgid ""
#~ "From this module, you can fine tune power management settings for each of "
#~ "your activities."
#~ msgstr ""
#~ "За допомогою цього модуля ви можете точно налаштувати параметри керування "
#~ "живленням для всіх ваших просторів дій."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Супровідник"
